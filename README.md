# SHOCKFIND Tool for FLASH AMR structure

This tool is based on the method described with in the paper

https://academic.oup.com/mnras/article/463/1/1026/2589921 by Lehmann et al

and was modified to take into account an AMR structure.

## Usage
To use this routine you'll have to project the AMR data onto a uniform grid
which can be done using the FLASH AMR TOOLS routine:
https://bitbucket.org/pierrenbg/flash-amr-tools/src

As inputs you'll need gas density, velocity and refinement level
of each cell. The example script of FLASH AMR TOOLS should 
provide you with these files in the right format and naming scheme.

Within the python script is an example given.

#### Visual

FLASH Dataset ----flash amr tools--> gas density, velocity and refinement 
level in .npy format

npy files ----shockfind--> 3D cube which marks shocked cells

## Output
The main output of this routine is a 3D numpy boolean array which represents
the shocked cells.