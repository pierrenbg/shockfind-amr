import numpy as np
import os
import scipy.constants as const
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, Normalize


# Calculate the central difference between two neighbour cells along an axis.
def grad(data, neigh_m, neigh_p, ccoords, axis, out):
	"""
	Determine the gradient of a 3D data cube along an axis. (AMR structure)

			Parameters:
					data (array(N, M, K)):          data cube from which the gradient is calcluated
					neigh_m (array(N, M, K)):       data cube which gives the previous neighbour
					neigh_p (array(N, M, K)):       data cube which gives the next neighbour
					ccoords (array(N, M, K)):       data cube containing cell coordinates of given axis
					axis (int):                     axis alogn the gradient is calculated
					out (array(N, M, K)):			array in which grad is saved
	"""
	print('Calculate gradient along axis %i' % axis)

	# grad = np.zeros_like(data, dtype=data.dtype)

	# tr = (neigh_m < 0)
	# tr *= (neigh_p < 0)

	ind = np.indices(data.shape, dtype=np.int16)

	ind[axis] = neigh_m
	# s = tuple([ind[i] for i in range(3)])

	tmp_dm = data[ind[0], ind[1], ind[2]]
	tmp_dm -= data
	tmp_cm = ccoords[ind[0], ind[1], ind[2]]
	tmp_cm -= ccoords
	# del s

	tmp_dm /= tmp_cm
	del tmp_cm

	out += 0.5 * tmp_dm
	del tmp_dm

	ind[axis] = neigh_p
	# s = tuple([ind[i] for i in range(3)])

	tmp_dp = data[ind[0], ind[1], ind[2]]
	tmp_dp -= data
	tmp_cp = ccoords[ind[0], ind[1], ind[2]]
	tmp_cp -= ccoords
	# del s

	tmp_dp /= tmp_cp
	del tmp_cp

	out += 0.5 * tmp_dp
	del tmp_dp, ind

	tr = (neigh_m < 0)
	tr += (neigh_p < 0)
	out[tr] = 0
	del tr


# Calculate the divergence of velocity
def divvel(vel, xmin, xmax, reflvl):
	print('Calculate divergence of velocity')
	dv = np.zeros(reflvl.shape, dtype=vel.dtype)
	for i in range(3):
		ccoord = cell_coords(xmin=xmin, xmax=xmax, reflvl=reflvl, axes=[i])
		left, right = get_neighbour_rlvl(reflvl=reflvl, axis=i)
		grad(data=vel[..., i], neigh_m=left, neigh_p=right, ccoords=ccoord, axis=i, out=dv)
		del left, right, ccoord
	return dv


# Calculate the gradient of density
def gradrho(dens, xmin, xmax, reflvl):
	print('Calculate gradient of density')
	shape = tuple(list(dens.shape) + [3])
	grho = np.zeros(shape, dtype=dens.dtype)
	for i in range(3):
		ccoord = cell_coords(xmin=xmin, xmax=xmax, reflvl=reflvl, axes=[i])
		left, right = get_neighbour_rlvl(reflvl=reflvl, axis=i)
		grad(data=dens, neigh_m=left, neigh_p=right, ccoords=ccoord, axis=i, out=grho[..., i])
		del left, right, ccoord
	return grho


# Determine the cell centers
# Needed for central difference method
def cell_coords(xmin, xmax, reflvl, axes=None):
	print('Calculate cell center coordinates')

	if isinstance(axes, type(None)):
		axes = range(3)
	elif isinstance(axes, int):
		axes = [axes]

	tr = len(axes) > 1

	if tr:
		tmp_shape = list(reflvl.shape)
		tmp_shape.append(len(axes))
		tmp_shape = tuple(tmp_shape)
	else:
		tmp_shape = reflvl.shape

	ccoords = np.zeros(tmp_shape, dtype=xmin.dtype)
	del tmp_shape

	for axis in axes:
		print('\tfor axis %i' % axis)
		shape = list(reflvl.shape)
		l_ax = shape.pop(axis)
		shape = tuple(shape)

		size = (xmax[axis] - xmin[axis]) / l_ax
		center = np.zeros(shape, dtype=xmin.dtype)

		cmin = np.zeros(shape, dtype=xmin.dtype)
		cmin[:] = xmin[axis]
		cmax = np.zeros(shape, dtype=xmin.dtype)
		cmax[:] = xmin[axis]

		s = [slice(0, reflvl.shape[0]), slice(0, reflvl.shape[1]), slice(0, reflvl.shape[2])]

		next_ind = np.zeros(shape, dtype=np.int16)

		for i in range(l_ax):
			tr_list = next_ind == i
			s[axis] = i
			select = tuple(s)

			if tr_list.sum() > 0:
				cmin[tr_list] = cmax[tr_list]
				cmax[tr_list] += 2. ** reflvl[select][tr_list] * size
				next_ind[tr_list] += 2 ** reflvl[select][tr_list]
				center[tr_list] = (cmin[tr_list] + cmax[tr_list]) / 2.

			if tr:
				ccoords[select][..., axis] = center
			else:
				ccoords[select][...] = center

	return ccoords


'''
# Determie the neighbour blocks from block coordinates.
# Does not differentiate for cells in a block.
def get_neighbour_coord(bcoord, axis):
	neigh_p = np.zeros_like(bcoord, dtype=np.int16)
	neigh_m = np.zeros_like(bcoord, dtype=np.int16)

	shape = list(bcoord.shape)
	l = shape.pop(axis)
	shape = tuple(shape)

	prev_ind = np.zeros(shape, dtype=np.int16) - 1
	next_ind = np.zeros(shape, dtype=np.int16) - 1

	s = [slice(0, bcoord.shape[0]), slice(0, bcoord.shape[1]), slice(0, bcoord.shape[2])]

	tmp_s_m = s[:]
	tmp_s_p = s[:]

	tmp_s_m[axis] = 0
	tmp_s_p[axis] = l - 1

	prev_coord = bx[tmp_s_m].copy()
	next_coord = bx[tmp_s_p].copy()

	for i in range(l):
		tmp_s_m[axis] = i
		tmp_s_p[axis] = l - 1 - i

		prev_true = prev_coord == bcoord[tmp_s_m]
		next_true = next_coord == bcoord[tmp_s_p]

		if np.bitwise_not(prev_true).sum() != 0:
			prev_ind[np.bitwise_not(prev_true)] = i
			prev_coord[np.bitwise_not(prev_true)] = bcoord[tmp_s_m][np.bitwise_not(prev_true)]

		if np.bitwise_not(next_true).sum() != 0:
			next_ind[np.bitwise_not(next_true)] = l - 1 - i
			next_coord[np.bitwise_not(next_true)] = bcoord[tmp_s_p][np.bitwise_not(next_true)]

		neigh_m[tmp_s_m] = prev_ind
		neigh_p[tmp_s_p] = next_ind

	return neigh_m, neigh_p
'''


# Deteremine the cell centers from the refinment levels of each cell.
def get_neighbour_rlvl(reflvl, axis):
	print('Determine neighbour cells along axis %i' % axis)
	neigh_p = np.zeros_like(reflvl, dtype=np.int16)
	neigh_m = np.zeros_like(reflvl, dtype=np.int16)

	shape = list(reflvl.shape)
	l_ax = shape.pop(axis)

	s = [slice(0, reflvl.shape[0]), slice(0, reflvl.shape[1]), slice(0, reflvl.shape[2])]

	tmp_s_m = s[:]
	tmp_s_p = s[:]

	tmp_s_m[axis] = 0
	tmp_s_p[axis] = l_ax - 1

	select_m = tuple(tmp_s_m)
	select_p = tuple(tmp_s_p)

	neigh_m[select_m] = 2 ** reflvl[select_m]
	prev_ind = 2 ** reflvl[select_m]
	neigh_p[select_p] = l_ax - 1 - 2 ** reflvl[select_p]
	next_ind = l_ax - 1 - 2 ** reflvl[select_p]

	for i in range(l_ax):
		tmp_s_m[axis] = i
		tmp_s_p[axis] = l_ax - 1 - i

		select_m = tuple(tmp_s_m)
		select_p = tuple(tmp_s_p)

		prev_true = prev_ind == i
		next_true = next_ind == l_ax - 1 - i

		if prev_true.sum() != 0:
			prev_ind[prev_true] += 2 ** reflvl[select_m][prev_true]

		if next_true.sum() != 0:
			next_ind[next_true] -= 2 ** reflvl[select_p][next_true]

		neigh_m[select_m] = prev_ind
		neigh_p[select_p] = next_ind

	neigh_m[neigh_m == l_ax] = -1

	return neigh_m, neigh_p


# Returns a boolean array which selects cells within a cylinder along a normal vector norm
# with radius r and height h. Shape is (2*n+1, 2*n+1, 2*n+1)
# Currently not used
def cylinder_true(norm, n, r, h):
	shape = (2 * n + 1, 2 * n + 1, 2 * n + 1)

	pos_tmp = np.indices(shape, dtype=np.float32) - n
	pos = np.zeros(tuple(list(shape) + [3]), dtype=np.float32)
	for i in range(3):
		pos[..., i] = pos_tmp[i]

	del pos_tmp
	norm_val = np.sqrt((norm ** 2).sum())
	norm /= norm_val

	par = np.dot(pos, norm)
	par_vec = par[..., None] * norm

	perp_vec = pos - par_vec
	perp = np.sqrt((perp_vec ** 2).sum(axis=3))

	par_tr = np.abs(par) <= h
	perp_tr = perp <= r

	par_p = np.zeros_like(par_tr, dtype=np.int8)
	par_p[par_tr * (par > h / 2.) * perp_tr] += 1
	par_m = np.zeros_like(par_tr, dtype=np.int8)
	par_m[par_tr * (par < -h / 2.) * perp_tr] += 1

	return par_tr * perp_tr, par_m, par_p


# Returns a boolean array which selects cells within a radius of r.
# Shaoe is (2*n+1, 2*n+1, 2*n+1)
# Currently not used
def sphere_true(n, r):
	shape = (2 * n + 1, 2 * n + 1, 2 * n + 1)

	pos_tmp = np.indices(shape, dtype=np.float32) - n
	pos = np.zeros(tuple(list(shape) + [3]), dtype=np.float32)
	for i in range(3):
		pos[..., i] = pos_tmp[i]

	del pos_tmp

	rad = np.sqrt((pos ** 2).sum(axis=3))
	rad_tr = rad < r
	return rad_tr


# Main routine for finding shocks
def find_shocks(dv, gr, reflvl, xmin, xmax, h, r, limit):
	# Pre process input data divergence of velocity and gradient of density
	dv = np.abs(dv)
	gr /= np.sqrt((gr ** 2).sum(axis=3))[..., None]
	gr = np.round(gr).astype(np.int8)

	# Filter cells above a specific limit
	x, y, z = np.where(dv >= limit)
	gr = gr[tuple([x, y, z])]

	# Find neighbour cells along density gradient (should be scaled with refinement
	# level otherwise only cell borders are detected
	neigh_x = np.asarray(get_neighbour_rlvl(reflvl=reflvl, axis=0))[tuple([slice(None), x, y, z])]
	tx = x.copy()
	gr_tr = gr[:, 0] != 0
	tx[gr_tr] = neigh_x[tuple([(gr[gr_tr, 0] + 1) / 2, np.where(gr_tr)[0]])]
	del neigh_x, gr_tr

	neigh_y = np.asarray(get_neighbour_rlvl(reflvl=reflvl, axis=1))[tuple([slice(None), tx, y, z])]
	ty = y.copy()
	gr_tr = gr[:, 1] != 0
	ty[gr_tr] = neigh_y[tuple([(gr[gr_tr, 1] + 1) / 2, np.where(gr_tr)[0]])]
	del neigh_y, gr_tr

	neigh_z = np.asarray(get_neighbour_rlvl(reflvl=reflvl, axis=2))[tuple([slice(None), tx, ty, z])]
	tz = z.copy()
	gr_tr = gr[:, 2] != 0
	tz[gr_tr] = neigh_z[tuple([(gr[gr_tr, 2] + 1) / 2, np.where(gr_tr)[0]])]
	del neigh_z, gr_tr

	'''
		vec = gr * 2**(reflvl[tuple([x, y, z])])[:, None]

		tx = x + vec[:, 0]
		ty = y + vec[:, 1]
		tz = z + vec[:, 2]
		del vec
	'''

	tr = dv[tuple([x, y, z])] > dv[tuple([tx, ty, tz])]
	del tx, ty, tz

	shock_cells = tuple([x[tr], y[tr], z[tr]])
	del x, y, z

	shock = np.zeros_like(dv, dtype=np.bool)
	shock[shock_cells] = True
	return shock


# Main function which checks for and prepares necessary files for shcokfind routine.
def main(xmin, xmax, h, r, limit, rewrite=False):
	reflvl = np.load('ref_level.npy').astype(np.int16)

	if not os.path.isfile('div_vel.npy') or rewrite:
		vel = np.load('gas_velocity.npy')
		dv = divvel(vel=vel, xmin=xmin, xmax=xmax, reflvl=reflvl)
		np.save(file='div_vel.npy', arr=dv)
		del vel, dv

	if not os.path.isfile('grad_rho.npy') or rewrite:
		dens = np.load('gas_density.npy').astype(np.float32)
		dens /= const.atomic_mass
		gr = gradrho(dens=dens, xmin=xmin, xmax=xmax, reflvl=reflvl)
		np.save(file='grad_rho.npy', arr=gr)
		del dens, gr

	dv = np.load('div_vel.npy')
	gr = np.load('grad_rho.npy')

	shock_tr = find_shocks(dv=dv, gr=gr, reflvl=reflvl, xmin=xmin, xmax=xmax, h=h, r=r, limit=limit)
	return shock_tr


def local_mach(gamma=5. / 3., rewrite=False):
	if os.path.isfile('local_mach.npy') and not rewrite:
		mach = np.load('local_mach.npy')
		return mach

	vel = np.load('gas_velocity.npy')

	# Absolute veloctiy in cm / s
	m = vel ** 2
	m = m.sum(axis=3)
	m **= 0.5

	del vel
	dens = np.load('gas_density.npy')
	press = np.load('pressure.npy')

	# Divide by local sound speed in cm / s
	m /= np.sqrt(gamma * press / dens)
	del dens, press

	np.save(arr=m, file='local_mach.npy')
	return m


# Not tested / implemented
def magnetic_mach():
	v_a = np.load('magnetic_field.npy')
	v_a **= 2
	v_a = v_a.sum(axis=3)
	v_a **= 0.5

	dens = np.load('gas_density.inp')
	dens **= 0.5

	v_a /= dens
	del dens
	return v_a


# Plots 4 rows with:
#   sum of shocked cells
#   normal mean of local mach nr.
#   mass weighted mean of local mach nr.
#   mean mach nr. in shocked cells
# alogn each line of sight
def plot_mach(shock_tr, xmin, xmax):
	mach = local_mach()
	dens = np.load('gas_density.npy')
	m_w_mach = mach * dens
	mach_tr = mach * shock_tr

	size = (xmax - xmin) / np.asarray(list(dens.shape))

	dx = (xmax - xmin)[0] / const.parsec * const.centi / 2
	ext = [-dx, dx, -dx, dx]

	fig, axes = plt.subplots(nrows=4, ncols=3, sharex='all', sharey='all', figsize=(6, 8))

	for i in range(3):
		im0 = axes[0, i].imshow(shock_tr.sum(axis=i), origin='lower', norm=Normalize(0, 500), extent=ext)
		im1 = axes[1, i].imshow(mach.mean(axis=i), origin='lower', extent=ext, norm=Normalize(0, 10))
		im2 = axes[2, i].imshow(
			m_w_mach.sum(axis=i) / dens.sum(axis=i), origin='lower', extent=ext, norm=Normalize(0, 10)
		)
		im3 = axes[3, i].imshow(
			np.divide(mach_tr.sum(axis=i), shock_tr.sum(axis=i), where=shock_tr.sum(axis=i) != 0),
			origin='lower', extent=ext, norm=Normalize(0, 30)
		)

	for ax in axes.flat:
		ax.set(xlabel='x [pc]', ylabel='y [pc]')

	for ax in axes.flat:
		ax.label_outer()

	plt.subplots_adjust(hspace=0, wspace=0)

	ax0 = axes[0, -1]
	cax0 = fig.add_axes([ax0.get_position().x1, ax0.get_position().y0, 0.02, ax0.get_position().height])
	fig.colorbar(im0, cax=cax0, label='# of shocked cells')

	ax1 = axes[1, -1]
	cax1 = fig.add_axes([ax1.get_position().x1, ax1.get_position().y0, 0.02, ax1.get_position().height])
	fig.colorbar(im1, cax=cax1, label=r'mean Mach nr$')

	ax2 = axes[2, -1]
	cax2 = fig.add_axes([ax2.get_position().x1, ax2.get_position().y0, 0.02, ax2.get_position().height])
	fig.colorbar(im2, cax=cax2, label='mean mass w. Mach nr')

	ax3 = axes[3, -1]
	cax3 = fig.add_axes([ax3.get_position().x1, ax3.get_position().y0, 0.02, ax3.get_position().height])
	fig.colorbar(im3, cax=cax3, label=r'mean Mach in shock$')

	fig.savefig('local_mach.png', dpi=300, bbox_inches="tight")
	plt.close(fig)


# Plots 2 2D histograms of local mach nr against gas density for whole box and shocked cells.
def plot_mach_hist(shock_tr):
	dens = np.load('gas_density.npy')
	dens = np.log10(dens)
	dens = dens.flatten()
	mach = local_mach()
	mach = mach.flatten()
	shock_tr = shock_tr.flatten()

	fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(3, 6))

	im0 = axes[0].hist2d(dens, mach, bins=100, range=[[-28, -20], [0, 40]], cmin=1, norm=LogNorm(vmin=1))
	im1 = axes[1].hist2d(
		dens * shock_tr, mach * shock_tr, bins=100, range=[[-28, -20], [0, 40]], cmin=1, norm=LogNorm(vmin=1)
	)

	ax0 = axes[0]
	cax0 = fig.add_axes([ax0.get_position().x1, ax0.get_position().y0, 0.02, ax0.get_position().height])
	fig.colorbar(im0[3], cax=cax0)

	ax1 = axes[1]
	cax1 = fig.add_axes([ax1.get_position().x1, ax1.get_position().y0, 0.02, ax1.get_position().height])
	if np.any(im1[0] > 0):
		fig.colorbar(im1[3], cax=cax1)

	plt.savefig('local_mach_hist.png', dpi=300, bbox_inches='tight')


# Plots 2 rows with:
#   nr of shocked cells
#   and column density
# alogn each line of sight.
def plot_shock_cdens(shock_tr, xmin, xmax):
	dens = np.load('gas_density.npy')
	size = (xmax - xmin) / np.asarray(list(dens.shape))

	dx = (xmax - xmin)[0] / const.parsec * const.centi / 2
	ext = [-dx, dx, -dx, dx]

	fig, axes = plt.subplots(nrows=2, ncols=3, sharex='all', sharey='all', figsize=(6, 4))

	for i in range(3):
		im0 = axes[0, i].imshow(shock_tr.sum(axis=i), origin='lower', norm=Normalize(0, 500), extent=ext)
		im1 = axes[1, i].imshow(
			dens.sum(axis=i) * size[i], origin='lower', norm=LogNorm(1e-4, 9e-1), extent=ext, cmap='inferno'
		)

	for ax in axes.flat:
		ax.set(xlabel='x [pc]', ylabel='y [pc]')

	for ax in axes.flat:
		ax.label_outer()

	# plt.tight_layout()
	plt.subplots_adjust(hspace=0, wspace=0)

	ax0 = axes[0, 2]
	cax0 = fig.add_axes([ax0.get_position().x1, ax0.get_position().y0, 0.02, ax0.get_position().height])
	fig.colorbar(im0, cax=cax0, label='# of shocked cells')

	ax1 = axes[1, 2]
	cax1 = fig.add_axes([ax1.get_position().x1, ax1.get_position().y0, 0.02, ax1.get_position().height])
	fig.colorbar(im1, cax=cax1, label=r'column density $\left[\mathrm{g\ cm}^{-2}\right]$')

	fig.savefig('shock_tr_cdens_new.png', dpi=300, bbox_inches="tight")
	plt.close(fig)


# Example
# Corner coordinates of the datasets
xmin = np.array([2.8931249e+20, -5.7862501e+20, -1.9287499e+20], dtype=np.float32)
xmax = np.array([6.7506249e+20, -1.9287501e+20, 1.9287499e+20], dtype=np.float32)

# Selection criteria for shockfind method.
# h and r currently not implemented.
# Limit might need to be changed for the specific simulation
h = 0
r = 0
limit = 5e-12
redo_shock = False
rewrite_grad = False

# Determine shocked cells and save as a boolean array.
if not os.path.isfile('shock_cells.npy') or redo_shock:
	shock_tr = main(xmin=xmin, xmax=xmax, h=h, r=r, limit=limit, rewrite=rewrite_grad)
	np.save(file='shock_cells.npy', arr=shock_tr)
else:
	shock_tr = np.load('shock_cells.npy')

# plot_shock_cdens(shock_tr, xmin, xmax)
# plot_mach(shock_tr, xmin, xmax)
# plot_mach_hist(shock_tr)
